\documentclass[10pt]{beamer}

\usetheme{metropolis}
\usepackage{appendixnumberbeamer}

\usepackage{booktabs}
%\usepackage[scale=2]{ccicons}

\newcommand{\themename}{\textbf{\textsc{metropolis}}}

\usepackage{multirow}
\usepackage{mathtools}

\usepackage{caption}
\captionsetup[figure]{labelformat=empty}

\usepackage{hyperref}
\renewcommand\UrlFont{\ttfamily\footnotesize}
\renewcommand{\thefootnote}{\arabic{footnote}}
\renewcommand{\footnoterule}{} % empty footnoterule

\makeatletter
\renewcommand{\verbatim@font}{\ttfamily\footnotesize}
\makeatother

\graphicspath{{assets/}}

\newcommand{\Prcond}[2]{\Pr\left[\, #1 \,\middle\vert\, #2 \,\right]}

\title{Reliability Analysis of Storage Systems}
\date{\today}
\author{{\bf John C F \\ \scriptsize 14305R006} \\ \textit{\footnotesize Under the guidance of} \\
\textbf{Prof. Varsha Apte}}
\institute{Indian Institute of Technology, Bombay}
\titlegraphic{\hfill\includegraphics[height=1.5cm]{iitb-logo.pdf}}

\usepackage{xcolor}
\newcommand\ytl[2]{
    \parbox[b]{0.18\linewidth}{\hfill{\color{cyan}\bfseries\sffamily #1}\kern 1em}\makebox[0pt][c]{$\bullet$}\vrule\quad \parbox[c]{0.79\linewidth}{\vspace{7pt}\color{red!40!black!80}\raggedright\sffamily #2\\[7pt]}\\[-3pt]}

\begin{document} %\uncover<2->

\maketitle

% ================================================================================

\section{Overview}

% --------------------------------------------------------------------------------

\begin{frame}{Long-term Objective}
    To create a general tool for use by storage system engineers for reliability analysis.
\end{frame}

% --------------------------------------------------------------------------------

\begin{frame}{State of the Art}
    \begin{itemize}
        \item Typically, analyses use Markov Models \cite{xin2003reliability, hafner2006notes,
            gopinath2009}
            \begin{itemize}
                \item Mostly good enough for comparison
                \item Known to give too optimistic estimates \cite{greenan2010mttm}
            \end{itemize}
        \item Lack of general (high-level) tools
    \end{itemize}
\end{frame}

% --------------------------------------------------------------------------------

\begin{frame}{Timeline (1/3)}
    \begin{minipage}[t]{\linewidth}
        \color{gray}
        \rule{\linewidth}{0.5pt}
        \ytl{Pre-MTP}{Explore storage system architectures}
        \ytl{}{Analyze simple storage systems with Markov Chains \\\small
               - Found that the model size can easily blow-up \\
               - Found it hard to build efficient abstractions}
        \ytl{}{Decides to try simulation\\\small - Utterly fails the first attempt}
        \ytl{}{Discovers Rare Event simulation (and HFRS \cite{greenan2009hfrs})\\\small
               - Reimplements HFRS with extensibility in mind}
        \ytl{}{\textbf{Decides to incorporate disk utilization into our failure model}\\\small
               Motivation: SSDs or a newer storage technology may display strong correlations
               between failures and utilization. So better be prepared for it.}
    \end{minipage}
\end{frame}

% --------------------------------------------------------------------------------

\begin{frame}{Timeline (2/3)}
    \begin{minipage}[t]{\linewidth}
        \color{gray}
        \ytl{}{Explores how utilization affects failures \\\small
               - Finds some evidence \cite{cole2000seagate, tyndall2013}}
        \rule{\linewidth}{0.5pt}
        \ytl{Stage 1}{Discovers that bivariate distributions can capture utilization-based
            effects on failures (in addition to normal ageing)}
        \ytl{}{Formulates a simple model using exponential marginal distributions}
        \ytl{}{Incorporates this in the reimplemented HFRS tool \\\small
               - Unsure whether our assumptions are correct \\
               - Decides to look for evidence from field data}
    \end{minipage}
\end{frame}

% --------------------------------------------------------------------------------

\begin{frame}{Timeline (3/3)}
    \begin{minipage}[t]{\linewidth}
        \color{gray}
        \ytl{}{Discovers a public dataset from Backblaze\footnotemark[1] \\\small
               - Analyzes this dataset for failure rate patterns\\
               - Insufficient data to analyze utilization}
        \rule{\linewidth}{0.5pt}
        \ytl{Stage 2}{NetApp opens the door to their vast ocean of data}
        \ytl{}{Does extensive analysis (this presentation)}
        \ytl{}{Discovers a mathematical error in our failure model.\\\small
               - Corrected the error, which increased the complexity and introduced a quirk.}
        \rule{\linewidth}{0.5pt}
    \end{minipage}
    %\vspace{2em}
    \footnotetext[1]{\scalebox{0.8}{\;\url{https://www.backblaze.com/b2/hard-drive-test-data.html}}}
\end{frame}

% ================================================================================

\section{Empirical Analysis}

% --------------------------------------------------------------------------------

\begin{frame}{Objective}
    To analyze the failure patterns in hard disks; in general, as well as at various levels of
    utilizations.

    More specifically,
    \begin{itemize}
        \item To compute the failure rate of hard disks over the age.
        \item To fit standard distributions on the failure data
            \begin{itemize}
                \item For use in our model, and for comparison with previous works.
            \end{itemize}
        \item To quantify the effects of utilization on failure rates (if any).
    \end{itemize}
\end{frame}

% --------------------------------------------------------------------------------

\begin{frame}{Related Work}
    \begin{itemize}
        \item Schroeder et al. \cite{schroeder2007disk} observed that failure rate exceeded data
            sheet spec for disks older than a year.
        \item Elerath et al. \cite{elerath2007enhanced} found that Weibull distribution is a
            good fit for disk failure data.
    \end{itemize}
    We'll use the above results for validating our own.
    \pause
    \begin{itemize}
        \item Pinheiro et al. \cite{pinheiro2007failure} observed no correlation between
            failures and utilization.
        \item Cole \cite{cole2000seagate}, Seagate, observed positive correlation between
            failure rates and a measure of disk utilization during RDTs.
    \end{itemize}
    Take away: Effects of utilization on disk failures in production environments is not
    well-studied.
\end{frame}

% --------------------------------------------------------------------------------

\begin{frame}{Terminology: AFR}
    \textbf{Annualized Failure Rate} (\textbf{AFR})
    \begin{itemize}
        \item Fraction of devices that is expected to fail during a year of use.
            \begin{itemize}
                \item Often expressed as a percentage value. Example:\\
                    100 failed out of 1000 in a year = 10\% AFR.
            \end{itemize}
        \item Just another unit of failure rate (``per-year'').
            \begin{itemize}
                \item Value greater than 100\% is meaningful. Example:\\
                    \vspace{-0.3em}
                    \begin{tabbing}
                    100 failed out of 1000 in a month \== 10\% failure rate per month\\
                                                       \>= 120\% AFR.
                    \end{tabbing}
            \end{itemize}
    \end{itemize}
    \footnotesize For more information on various definitions (and misdefinitions), and their
    measurement procedures, see \cite{elerath2000afr}.
\end{frame}

% --------------------------------------------------------------------------------

\begin{frame}{Terminology: Hazard Function}
    Hazard Function a.k.a Hazard Rate a.k.a Failure Rate is a property of a probability
    distribution, defined by:
    \[
        h_T(t) = \frac{f_T(t)}{1-F_T(t)}
    \]

    Within the scope of our discussion, we use ``Hazard Function'' only when a probability
    distribution is involved, while ``Failure Rate'' is used in a more general sense.
\end{frame}

% --------------------------------------------------------------------------------

\begin{frame}[fragile]{Data Preprocessing}
    NetApp telemetry processes collect hardware and software logs.

    From these logs, we were able to extract data of a single hard disk model into the
    following form:

    \begin{columns}[T,onlytextwidth]
        \footnotesize
        \column{0.7\textwidth}
        \begin{table}
            \begin{tabular}{cccc}
                \hline
                Serial* & Datestamp* & Power-on Hrs & GBs transf. \\
                \hline
                XYZ  & 2017-01-02 & 23000 & 345 \\
                XYZ  & 2017-01-03 & 23024 & 385 \\
                XYZ  & 2017-01-04 & 23048 & 425 \\
                ABC  & 2017-03-15 &  1200 &  65 \\
            \end{tabular}
            \caption*{Disk logs}
        \end{table}

        \column{0.29\textwidth}
        \begin{table}
            \begin{tabular}{cc}
                \hline
                Serial* & Datestamp \\
                \hline
                XYZ  & 2017-01-04 \\
                ABC  & 2017-03-15 \\
            \end{tabular}
            \caption*{Disk failures}
        \end{table}
    \end{columns}

    \footnotesize Fun fact: The total number of unique serial numbers was nearly half a million.
\end{frame}

% --------------------------------------------------------------------------------

\begin{frame}{The Strategy}
    \begin{itemize}
        \item We can't directly calculate CDF w.r.t age solely from a list of failures (and
            corresponding ages) because such a method would require that:
            \begin{itemize}
                \item The total number of disks (incl. surviving ones) be known.
                \item The age at which they came under our observation should be zero.
                \item All disks must remain under our observation unless they fail.
            \end{itemize}
            \pause
        \item We can calculate failure rate if we know number of disks failed during a span of
            age, as well as the number of disks under observation during that same span.
            \begin{itemize}
                \item We could then fit the hazard function of a standard distribution.
            \end{itemize}
    \end{itemize}
\end{frame}

% --------------------------------------------------------------------------------

\begin{frame}{Failure Rate Computation (1/4)}
    \begin{equation*}
        \text{AFR(in first year of age)} = \frac{\text{\# disks failed < 1 y/o}}
                                                {\text{\# disks < 1 y/o}}
    \end{equation*}

    \textbf{Problem}: If we have a population of {\bf 1000} disks, where {\bf 100} were deployed
    at age {\bf zero}, and remaining {\bf 900} at age {\bf 11 mo}, then the denominator would be
    overrepresented at 1000. {\small(Since the numerator would have been much higher if all 1000
    were deployed at age zero.)}

    \pause
    \textbf{Solution}: Use {\bf disk-years} instead of simple counting in the denominator.
    Hence, the new denominator would be $100 + 900 \times 1/12 = 175$ disk-years.
\end{frame}

% --------------------------------------------------------------------------------

\begin{frame}{Failure Rate Computation (2/4)}
    \begin{equation*}
        \text{AFR(in an age-range)} = \frac{\text{\# disks failed in age-range}}
                                           {\text{\# disk-years in age-range}}
    \end{equation*}

    \pause
    \begin{columns}[T,onlytextwidth]
        \column{0.48\textwidth}
        \begin{figure}
            \includegraphics[width=\textwidth]{fr-step-1.pdf}
        \end{figure}

        \column{0.48\textwidth}
        \begin{figure}
            \includegraphics[width=\textwidth]{fr-step-2.pdf}
        \end{figure}
    \end{columns}

    \begin{columns}[T,onlytextwidth]
        \column{0.48\textwidth}
        \begin{figure}
            \includegraphics[width=\textwidth]{fr-step-3.pdf}
        \end{figure}

        \column{0.48\textwidth}
        \vspace{2em}
        \footnotesize
        Points of note:
        \begin{itemize}
            \item The observation in \cite{schroeder2007disk} holds.
            \item Only 4 weeks of logs were used for construction of this graph.
        \end{itemize}
    \end{columns}
\end{frame}

% --------------------------------------------------------------------------------

\begin{frame}{Failure Rate Computation (3/4)}
    \begin{equation*}
        \text{AFR($t$)} = \frac{C'(t)}{N(t)}
    \end{equation*}

    \begin{tabbing}
        \hspace{1.5cm}\=\kill
        where \> $C(t)$ is the cumulative number of failures at age $t$,\\
              \> $C'(t)$ \= is the derivative of $C(t)$ (making it kind of \\
              \>         \> the ``net" failure rate due to all disks at age $t$),\\
        and   \> $N(t)$ \= is the number of disks that were seen at age $t$.
    \end{tabbing}

    The above equation can be derived as the limit of the previous equation when |age-range|
    $\rightarrow$ 0.
\end{frame}

% --------------------------------------------------------------------------------

\begin{frame}{Failure Rate Computation (4/4)}
    \begin{columns}[T,onlytextwidth]
        \column{0.48\textwidth}
        \begin{figure}
            \includegraphics[width=\textwidth]{fr-cont-pre.pdf}
        \end{figure}

        \column{0.48\textwidth}
        \begin{figure}
            \includegraphics[width=\textwidth]{fr-cont.pdf}
        \end{figure}
    \end{columns}

    \vspace{0.8em}
    \begin{columns}[T,onlytextwidth]
        \column{0.48\textwidth}
        \vspace{1em}
        \footnotesize
        Points of note:
        \begin{itemize}
            \item Here, used 14 weeks of logs.
            \item There are matching valleys and peaks in $C'(t)$ and $N(t)$, which disappears
                in the AFR curve.
            \pause
            \item Note that the ``discrete'' graph approaches the ``continuous'' one.
        \end{itemize}

        \column{0.48\textwidth}
        \begin{figure}
            \includegraphics[width=\textwidth]{fr-limit-2.pdf}
        \end{figure}
    \end{columns}
\end{frame}

% --------------------------------------------------------------------------------

\begin{frame}{Fitting Theoretical Distributions (1/2)}
    \textbf{Proposal}: Fit the hazard function of a distribution on the empirical failure rate curve.

    \textbf{Problem}: Points where only a few disks were under observation might be outliers.

    \pause
    \textbf{Solution}: Instead of simple least squares method, use weighted least squares to fit the
    curve with the weights being $N(t)$.
\end{frame}

% --------------------------------------------------------------------------------

\begin{frame}{Fitting Theoretical Distributions (2/2)}
    \begin{columns}[T,onlytextwidth]
        \column{0.48\textwidth}
        \begin{figure}
            \includegraphics[width=\textwidth]{dist-lfit.pdf}
        \end{figure}

        \column{0.48\textwidth}
        \vspace{2em}
        \scriptsize
        \[
            {\rm Minimize} \sum_i w_i \times ( \hat{h}_i - h_T(t_i) )^2
        \]
        \begin{tabular}{cccc}
                       & Parameters                  & Mean & Loss \\
            \hline
            Weibull    & $k$ = 2.8, $\lambda$ = 8.6  & 7.6y & 0.26 \\
            Gamma      & $k$ = 4.8, $\theta$ = 1.7   & 8.4y & 0.33 \\
            Lognormal  & $\sigma$ = 0.5, $\mu$ = 2.1 & 9.2y & 0.40 \\
        \end{tabular}

        \begin{onlyenv}<1-2>
            \pause
            \vspace{2em}
            Points of note:
            \begin{itemize}
                \item Weibull shape > 2 (meaning accelerating failure rate). This is far beyond
                    what was reported in \cite{elerath2007enhanced} where $0.9 \le k \le 1.5$.
                \item Mean time to failure < 10 years. There were a few cases where
                    \cite{elerath2007enhanced} reported a mean < 10 years, but majority
                    were greater than that (up to about 50 years).
            \end{itemize}
            \vspace{2.63em} %extra space
        \end{onlyenv}
    \end{columns}

    \begin{onlyenv}<3->
        \begin{columns}[T,onlytextwidth]
            \column{0.48\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{dist-fit.pdf}
            \end{figure}

            \column{0.48\textwidth}
            \begin{figure}
                \includegraphics[width=\textwidth]{dist-llfit.pdf}
            \end{figure}
        \end{columns}
    \end{onlyenv}
\end{frame}

% --------------------------------------------------------------------------------

\begin{frame}{Utilization Analysis}
    \textbf{Objective}: Check for differences in failure patterns when I/O load on disks is ``low''
    compared to when it is ``high''.

    Due to lack of significant number of diversely loaded disks available, we had to limit to
    the following two buckets:

    {\bf Low load}: 0-1.6 GB/hr\\
    {\bf High load}: 4-16 GB/hr

    The load (avg) for an `entity' in the bucket is calculated as follows:
    \[
        \rm \frac{max(\text{GBs transferred}) - min(\text{GBs transferred})}
                 {max(\text{Power-on hours}) - min(\text{Power-on hours})}
    \]
\end{frame}

% --------------------------------------------------------------------------------

\begin{frame}{Utilization Analysis: Long-term}
    Entity: Unique hard disk (identified by unique serial number)

    \begin{figure}
        \includegraphics[width=0.6\textwidth]{cors-twin.pdf}
    \end{figure}

    Conclusion: Positive correlation not observed.
\end{frame}

% --------------------------------------------------------------------------------

\begin{frame}{Utilization Analysis: Short-term}
    Entity: Serial number $\times$ `Week-stamp'

    \only<1>{
    \footnotesize
    \begin{figure}
        \begin{tabular}{cc|cc|c|c}
            \hline
            \multicolumn{2}{c|}{Entity*}
                        & \multicolumn{2}{c|}{Power-on hours}
                                       & Avg. Load & \multirow{2}{*}{Bucket} \\
            Serial  & Weekstamp & Min   & Max  & (GB/hr)   & \\
            \hline
            ABC     & 2         & 400   & 560  & 0.4       & Low \\
            ABC     & 3         & 560   & 730  & 9.2       & High \\
            ABC     & 4         & 730   & 900  & 3.6       & -- \\
            ABC     & 5         & 900   & 1060 & 8.3       & High \\
            ABC     & 6         & 1060  & 1190 & 1.0       & Low \\
        \end{tabular}
    \end{figure}
    }

    \only<2>{
    \begin{columns}[T,onlytextwidth]
        \column{0.48\textwidth}
        \begin{figure}
            \includegraphics[width=\textwidth]{fine-twin.pdf}
        \end{figure}

        \column{0.48\textwidth}
        \begin{figure}
            \includegraphics[width=\textwidth]{fine-twin2.pdf}
        \end{figure}
    \end{columns}

    Conclusion: Positive correlation too high. Most likely to have a different common cause.
    }
\end{frame}

% ================================================================================

\section{Conclusions}

% --------------------------------------------------------------------------------

\begin{frame}{Summary}
    \begin{itemize}
        \item The method we presented tries to make efficient use of available data.
        \item Failure rate of hard disks was observed to be strictly increasing (and
            accelerating) with age, contradicting popular exponential assumptions.
        \item Correlation between utilization and failure rate needs further investigation.
    \end{itemize}
\end{frame}

\begin{frame}{Self Assessment: The Goal Not Achieved}
    The Goal, ``a generic tool for all,'' might have been too ambitious.
    \begin{itemize}
        \item The systems we are trying to abstract are highly complex and hard to break into
            reusable ``atomic" units.
        \item The effects of many mechanisms, done at the software level, are hard to quantify.
    \end{itemize}
\end{frame}

\begin{frame}{Self Assessment: Absence of Reference Points}
    \begin{itemize}
        \item We found no other tool or field data with which we can validate our tool, even
            prior to utilization modeling (except HFRS itself, which was ``validated'' by
            comparing against Markov models).
        \item We found no data to validate our utilization model (and its underlying
            assumptions).
        \item We cannot expect to find any field data of storage system failures, since hard
            disks themselves are hard to analyze, let alone even a simple storage system.
        \item So how do we prove correctness?
    \end{itemize}
\end{frame}

\begin{frame}{Self Assessment: Question of Utility}
    \begin{itemize}
        \item Even if we ignore the previous points, will the tool really be useful to the
            engineers?
            \begin{itemize}
                \item As it is now, the engineers seem to trust Markov models and gut feeling
                    [need citation].
                \item So would this new tool which might require significant time-investment
                    pose as a good alternative?
            \end{itemize}
        \item Needed more inputs from the industry experts about the current practices on
            storage analysis (and perhaps improve upon that).
    \end{itemize}
\end{frame}

% ================================================================================

\begin{frame}[standout]
    Thank You!

    \small Questions?
\end{frame}

% ================================================================================

\section{Theoretical Analysis}

% --------------------------------------------------------------------------------

\begin{frame}{Simple Simulation (1/4)}
    Determining unreliability through simulation:
    \[
        \text{Unreliability} = \frac{\text{\# runs that lost data}}
                                    {\text{\# runs in total}}
    \]
\end{frame}

% --------------------------------------------------------------------------------

\begin{frame}{Simple Simulation (2/4)}
    Step 1: Initialize lifetimes
    \begin{figure}
        \includegraphics[width=0.7\textwidth]{sim-1.pdf}
    \end{figure}
\end{frame}

% --------------------------------------------------------------------------------

\begin{frame}{Simple Simulation (3/4)}
    Step 2: Jump to the next event (failure => schedule repair)
    \begin{figure}
        \includegraphics[width=0.7\textwidth]{sim-2.pdf}
    \end{figure}
\end{frame}

% --------------------------------------------------------------------------------

\begin{frame}{Simple Simulation (4/4)}
    \vspace{3.28em}

    Step 3: Jump to the next event (repair => assign lifetime)
    \begin{figure}
        \includegraphics[width=0.7\textwidth]{sim-3.pdf}
    \end{figure}

    And repeat till either MAXTIME is reached, or the system is unrecoverable.
\end{frame}

% --------------------------------------------------------------------------------

\begin{frame}{Incorporating Utilization}
    Use an extra random variable, in addition to the one denoting the age of failure ($T$),
    which would denote the total I/O done by the time of failure ($X$).

    During simulation, a value that corresponds to $X$ can be kept track of that, like $T$, is
    non-decreasing.

    \vspace{-1em}
    \begin{columns}[T,onlytextwidth]
        \column{0.6\textwidth}
        \vspace{3em}
        Question 1: When should a disk fail?

        Question 2: How is the failure rate affected by rate of I/O?

        Assume: Exponential marginal distributions.

        \column{0.38\textwidth}
        \begin{figure}
            \includegraphics[width=\textwidth]{age-io-path.pdf}
        \end{figure}
    \end{columns}
\end{frame}

% --------------------------------------------------------------------------------

\begin{frame}{Utilization: Failing a Disk}
    Problem: Cannot use the method of sampling a point using the failure distribution and simply
    jump to the ``closest'' point. Because the points are in 2D and the system controls the
    slope.

    \begin{columns}[T,onlytextwidth]
        \column{0.6\textwidth}
        \vspace{2em}
        Solution: Use the survival function as an estimate of disk health. Assign a minimum
        disk-health ($S_{\rm min}$) to each disk, and fast-forward the system until a disk hits
        its minimum-health (survival starts from 1 and decreases).

        \column{0.38\textwidth}
        \begin{figure}
            \includegraphics[width=\textwidth]{age-io-surv.pdf}
        \end{figure}
    \end{columns}
\end{frame}

% --------------------------------------------------------------------------------

\begin{frame}{Utilization: Effect on Failure Rate (1/2)}
    Question: Given a particular I/O load on a disk, how is the failure rate affected.

    Answer: Introduce a random variable $W = X/T$. Find the hazard rate with respect to $T$
    conditioned on $W = w$.
    \begin{align*}
        h_{T|W}(t|w) &= \frac{f_{T|W}(t|w)}{S_{T|W}(t|w)} \\
                     &= \frac{t(\lambda + \theta w)^2}{1+(\lambda + \theta w)t} \\
        \lim_{t\rightarrow\infty} h_{T|W}(t|w) &= \lambda + \theta w
    \end{align*}
\end{frame}

% --------------------------------------------------------------------------------

\begin{frame}{Utilization: Effect on Failure Rate (2/2)}
    Issue: When the load on the disk changes after a change in system state, the hazard rate
    restarts from zero.

    \begin{columns}[T,onlytextwidth]
        \column{0.6\textwidth}
        \vspace{2em}
        Solution: Unknown

        \column{0.38\textwidth}
        \begin{figure}
            \includegraphics[width=\textwidth]{age-io-path-change.pdf}
        \end{figure}
    \end{columns}
\end{frame}

% ================================================================================

\begin{frame}[standout]
    That's all folks!
\end{frame}

% ================================================================================

\appendix

% ================================================================================

\begin{frame}[allowframebreaks]{References}
    \bibliography{main}
    \bibliographystyle{alpha}
\end{frame}

% Theme attribution
\begin{frame}
    \begin{center}
        {\large \themename \sc ~theme}

        \url{github.com/matze/mtheme}

        Licensed under \href{http://creativecommons.org/licenses/by-sa/4.0/}{CC BY-SA 4.0}.

        \vspace{2mm}
        %\ccbysa
    \end{center}
\end{frame}

\end{document}
