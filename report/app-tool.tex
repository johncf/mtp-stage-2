\chapter{Simulator Implementation}\label{app:tool}

Our algorithm is very similar to that of HFRS \cite{greenan2009hfrs}. Currently, our tool can only
be used to estimate the reliability of an MDS scheme (discussed in chapter \ref{ch:imp-samp}) under
various failure rates, repair rates, and system load.

\section{Input Parameters}

\begin{itemize}
    \item MDS configuration $(n, k)$
    \item Disk failure distribution parameters: $(\lambda, \theta)$ in bivariate distribution over
        $(T, X)$ where $T \sim EXP(\lambda)$ and $X \sim EXP(\theta)$.
    \item Disk repair distribution parameters: Weibull(scale, shape, offset)
    \item Mission time
    \item Mean system load ($dx/dt$)
    \item Repair rate ($dx/dt$) --- the rate at which a failed disk is reconstructed --- results in
        additional load on disks when there are failures
    \item Importance sampling parameter ($\beta$ as in section \ref{sec:ubfb} or
        \ref{sec:stor-is-theory})
\end{itemize}

\section{Algorithms}

The simulation has two modes of operation: ``Normal'' mode -- when there are no failures, and
``Degraded'' mode -- when there are one or more failures. In Normal mode, we use the original
failure distribution to simulate failures, without using any importance sampling technique. In
Degraded mode, the disk failures are simulated using Uniformized Balanced Failure Biasing (section
\ref{sec:ubfb}), until either the system experiences a data loss or recovers completely.

\subsection{\texttt{simulate}}

Inputs: \texttt{sys\_plan}, $\beta$, $T_{mission}$, $n$

\begin{itemize}
    \item $L_{total} \leftarrow 0$
    \item Sampling loop -- repeat $n$ times:
        \begin{itemize}
            \item $L \leftarrow 1$
            \item \texttt{system} $\leftarrow$ create system using plan \texttt{sys\_plan}.
            \item Simulation loop -- until \texttt{system} time $\geq T_{mission}$:
                \begin{itemize}
                    \item $L_{factor} \leftarrow$ \texttt{next\_state(system, $\beta$)}
                    \item $L \leftarrow L * L_{factor}$
                    \item if \texttt{system} has lost data:
                        \begin{itemize}
                            \item $L_{total} \leftarrow L_{total} + L$
                            \item break simulation loop
                        \end{itemize}
                \end{itemize}
        \end{itemize}
    \item \underline{Return} $L_{total}/n$
\end{itemize}

\subsection{\texttt{next\_state}}

Inputs: \texttt{system}, $\beta$

\begin{itemize}
    \item \texttt{load\_dist} $\leftarrow$ compute load distribution across disks in the current
        state of \texttt{system} (based on redundancy logic, system load, and repair rate)
    \item if \texttt{system} has no failures: // Normal mode
        \begin{itemize}
            \item Fail a disk and advance \texttt{system} time as described by equation \ref{eq:fff}
            \item \underline{Return} 1
        \end{itemize}
    \item else: // Degraded mode
        \begin{itemize}
            \item $t_{rare} \leftarrow$ sample from $EXP(\beta)$
            \item if $t_{rare} <$ next repair time: // failure or pseudo
                \begin{itemize}
                    \item Advance \texttt{system} time to $t_{rare}$
                    \item Choose this event as a failure with probability $P_{fb} = 0.8$.
                    \item Compute $L_n$ from equation \ref{eq:ubfb-lr} where $\lambda_j(t_n)$ is
                        computed using equation \ref{eq:bivar-cond-h} where $\theta$ can be
                        obtained from \texttt{load\_dist}. \footnote{Our implementation used the
                        expression which equation \ref{eq:bivar-cond-h} asymptotes to instead of
                        using it directly.}
                    \item \underline{Return} $L_n$
                \end{itemize}
            \item else: // repair event
                \begin{itemize}
                    \item Repair the next disk in queue
                    \item Advance \texttt{system} accordingly
                    \item \underline{Return} 1
                \end{itemize}
        \end{itemize}
\end{itemize}

\section{Data Structure Design}

\begin{figure}
\centering
\includegraphics[width=\textwidth]{stosim-ds.eps}
\caption{Simulator data structure design}
\label{fig:stosim-ds}
\end{figure}

The tool was implemented using Rust, which is a compiled language with a powerful type system so as
to provide type- and memory-safety guarantees right at compile-time. We made heavy use of the
language type-system to ensure extensibility and correctness. Figure \ref{fig:stosim-ds}
illustrates a simplified view of some of the data structures and interfaces used in the tool. Blue
color indicates concrete types and green color indicates generic types. \texttt{SimpleSystem} is a
concrete type implementing the \texttt{StorageSystem} interface (or ``trait'' as it is called in
Rust). Any type which implements the \texttt{StorageSystem} trait can be passed to a function
called \texttt{simulate} so as to simulate and analyze the reliability of the system, returning the
unreliability value (probability of data loss within mission time).

\section{Test Results}

The tool takes in input through stdin, formatted as json, and gives json formatted output
representing the unreliability value.

Below is a run where the simulator analyzes MDS(6, 2) configuration. The range represented by
plus-or-minus symbol (±) in the output denote 95\% confidence interval.

\begin{verbatim}
//input
{
    "fail_dist": [4e-5, 2e-6],
    "repr_dist": [12, 2, 6],
    "mds_conf": [6, 2],
    "system_load": 40,
    "repair_load": 5,
    "mission_time": 10000,
    "repeat": 5000
}

//output
Unreliability: 2.012e-5 ± 4.985e-6
\end{verbatim}

Below is another run with MDS(7, 3) as the configuration. The speedup parameter is also higher,
otherwise the system may not experience any failures.

\begin{verbatim}
//input
{
    "fail_dist": [4e-5, 2e-6],
    "repr_dist": [12, 2, 6],
    "mds_conf": [7, 3],
    "system_load": 40,
    "repair_load": 5,
    "mission_time": 10000,
    "repeat": 5000
}

//output
Unreliability: 1.280e-7 ± 3.540e-8
\end{verbatim}
