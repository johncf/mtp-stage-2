\chapter{Background}

\section{Replication Schemes for Fault Tolerance}\label{sec:redundancy-techs}

Modern storage systems use sophisticated and multi-level replication mechanisms to achieve very
high reliability while maximizing storage efficiency. Here we will look at some of the most basic
replications schemes in use.

Let's consider that we want to store 9 blocks of data: [A1, A2, A3, B1, ... C3] in that order. We
define \textit{replication factor} as the ratio between the size taken up for storing a piece of
data to the size of the original data.

\subsection{Simple Mirroring}

This is the simplest redundancy mechanism, in which exact copies (each copy is called a
\textit{mirror}) of the original data is stored in two or more disks. This technique is also called
RAID 1. The replication factor in this technique is same as the number of mirrors. Here, read
requests can be distributed among the disks, but writes need to be replicated to all disks.

\begin{figure}[h]
\centering
\includegraphics[height=60mm]{raid1.eps}
\caption{RAID 1}
\label{fig:raid4}
\end{figure}

\subsection{XOR-based}

In this scheme, one disk worth of data is computed from $n$ disks of data by XOR-ing them together.
Take a look at the RAID 4 scheme illustrated in figure \ref{fig:raid4}. As an example, the block
$\rm A_p$ is computed as follows:

\[ \rm A_p = A1 \oplus A2 \oplus A3 \]

\begin{figure}[h]
\centering
\includegraphics[height=48mm]{raid4.eps}
\caption{RAID 4}
\label{fig:raid4}
\end{figure}

Now, if Disk 3 fails, then A3, and similarly B3 and C3, can be recovered by:

\[ \rm A3 = A1 \oplus A2 \oplus A_p \]

Therefore, as long as $n$ out of the total $n + 1$ is working, all data can be recovered. Note
that consecutive blocks are laid out one disk after another, thus reads and writes can be done in
parallel (at three times the speed in our example).

In this scheme, the replication factor is $\frac{n + 1}{n}$.

\subsection{Reed-Solomon Codes}

This is a generalization of RAID schemes. In this, $k$ ``parity'' drives are added to a set of $n$
``data'' drives such that it can recover from \textit{any} combination of $k$ failures. The
mathematical theory behind this technique is based on univariate polynomials over finite fields.

Reed-Solomon codes fall in the family of Maximum Distance Separable (MDS) codes which have the
greatest error correcting ability among linear codes. Hereafter, ``MDS($n$, $k$)'' will be used to
refer to such a scheme.

\begin{figure}[h]
\centering
\includegraphics[height=48mm]{raid6.eps}
\caption{MDS(3, 2)}
\end{figure}

The replication factor for an MDS($n$, $k$) scheme is $\frac{n + k}{n}$.

\subsection{Heterogeneous Codes}

Heterogeneous codes are trades repair costs with storage overhead for a given target reliability.
If there are $k$ additional drives used, then the system can tolerate ``almost'' $k$ failures. This
is because some failures are more ``costly'' than others, such that failure of a combination of
these costlier ones along with a particular set of data drives may result in an early data loss
(before $k$ drives fail). While on the other hand, most of the failures will be very cheap in that
their reconstructions will involve less than $k$ disks, which implies less bandwidth needed for
those repairs compared to MDS codes.

\section{Simple Reliability Analysis}\label{ssec:sim-base}

A popular method of reliability analysis of simple systems is to use a Markov Model
\cite{xin2003reliability, hafner2006notes, gopinath2009}. These models are generally good at
comparing the reliability of relatively similar systems. But due to the Markovian assumption
(memorylessness property in a Poisson process), the absolute results from the model (such as Mean
Time to Data Loss) are known to be too optimistic \cite{greenan2010mttm}; since disks in real-world
are observed to show complex failure patterns, most notably showing an increased failure rate as
they age \cite{pinheiro2007failure, schroeder2007disk, yang1999comprehensive, elerath2007enhanced}.
Therefore we decided to use simulation in the hopes of capturing the behavior of disks and the
system more accurately, so as to obtain results that are more realistic.

In this section, we describe how to analyze reliability of a storage system using a naive
simulation approach, as well as the drawbacks of it. Say, we have a system of 5 disks, and it can
tolerate up to 2 failures without data loss. It is known that all disks have the same failure
characteristics, defined by some probability distribution over age. When a device fails, a recovery
process is started which will eventually restore the system to a state without failures, unless
more failures occur within that time period. This repair process is also stochastic and a
probability distribution over time-to-repair is known. The goal is to estimate the probability of
data loss from the system within a given mission time, $T$. Then, the reliability estimation would
be done as follows:

\begin{itemize}
    \item Run the simulation $N$ times, and record the number of runs which ended in data loss,
        $N_f$.
    \item Provided that enough data loss events were observed, the reliability within mission time
        $T$ is given by:
        \begin{equation}\label{eq:data-loss}
            \Pr(\text{data loss}) = \frac{N_f}{N}
        \end{equation}
\end{itemize}

\noindent Now, a simulation run proceeds as follows:

\begin{enumerate}
    \item Initialize simulation time, $t \leftarrow 0$.
    \item For each disk in system, sample an age $a$ from the given failure probability
        distribution and schedule that disk's failure event at $t_f = t + a$.
    \item Move the system simulation time forward to the first event among the pending ones (disk
        failures and repairs).
    \item If system time $t >= T$:
        \begin{itemize}
            \item Stop simulation (no data loss)
        \end{itemize}
    \item If the event was a failure event,
        \begin{itemize}
            \item If the current failure resulted in loss of data, stop simulation
            \item Else, using the time-to-repair distribution, schedule a repair event 
        \end{itemize}
    \item Else if the event was a repair event,
        \begin{itemize}
            \item A failed disk has been replaced with a new disk (populated with expected data).
                Thus sample a new age from the failure probability distribution and schedule a new
                failure event (as in step 2).
        \end{itemize}
    \item Go to step 3.
\end{enumerate}

The above pseudo-code illustrates the basic steps in a simple storage system simulation. The
important thing to note here is that even though it is a storage system, we did not take data
transfers into account. This is because according to our model data transfers do not affect
failure events (or repair events) in any way. Everything was purely time-dependent processes.

As we mentioned before, this method is naive and inefficient. From equation \ref{eq:data-loss}, it
is clear that if our system is highly reliable that the probability of data loss is, say,
$10^{-9}$, then we will have to repeat the simulation significantly more than a billion times to
obtain results with confidence. To solve this problem when dealing with such rare events, a
technique known as Importance Sampling is generally used, which we will discuss in chapter
\ref{ch:imp-samp}.
