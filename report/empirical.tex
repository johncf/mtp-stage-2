\chapter{Empirical Disk Failure Analysis}\label{ch:empirical}

Failure rates of most devices, including disks, are generally assumed to follow a bath-tub curve as
shown in figure \ref{fig:bathtub}. It is effectively a combination of 3 failure rate curves: (1) an
infant mortality rate which is high towards the beginning of life, (2) a base failure rate which
stays constant throughout lifetime, and (3) an end-of-life wear-out rate which increases as the
disk gets significantly old. Even though this is a well-accepted model, this is typically not
observed in the storage industry. This is because most infant mortality failures are caught by hard
disk manufacturers during the testing phase before they are deployed in production.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{bathtub.eps}
    \caption{Bath-tub curve \cite{yang1999comprehensive}}\label{fig:bathtub}
\end{figure}

The most commonly used measure of failure rate is Annualized Failure Rate (AFR). AFR is the
fraction of devices that is expected to fail during a year of use. Therefore, AFR is often
expressed as a percentage value. Note that even though AFR is defined in terms of years, it is just
a unit of failure rate. For instance, if 10\% of devices in a certain population are expected to
fail per month, the AFR for that device is 1.2 (or 120\%). A failure rate of 120\% may seem to make
no practical sense at first, but consider the following scenario. We have 100 active devices out of
which 10\% are expected to fail every month. If we replace every failed device with a working one
without delays, then we are expected to witness 120 failures by the end of that year.

% TODO add a list of contributions?

\section{Related Work}

Schroeder et al. \cite{schroeder2007disk} studied disk replacement rates over 5 years. They noted
that datasheet specified Mean Time To Failure (MTTF) of hard disks (most often above 1,000,000
hours) suggests an annual failure rate of at most 0.88\%, but the actual failure rate they observed
typically exceeds 1\%, with 2-4\% being commonplace. They specifically noted that during the first
year of deployment, disk failure rate did fall within the data sheet range, but beyond first year,
failure rates were observed to be steadily increasing with age. Our observations were in agreement
with this, as we shall see in section \ref{sec:afr}. It is also worth mentioning that they observed
very little infant mortality rate (compared to wear-out), and similarly did we.

% Since failure rate was found to be dependent on age, we decided not to analyze Mean Time Between
% Failures (MTBF) since it would be greatly affected by the age distribution within the population.

Cole et al. \cite{cole2000seagate} from Seagate studied Mean Time Between Failures (MTBF) and
Annualized Failure Rate (AFR) patterns in disk drives during reliability demonstration tests
(RDTs). They observed that both MTBF and AFR showed positive correlation with Duty Cycles (disk
utilization). They also noted that MTBF dropped as disks aged indicating diminishing infant
mortality failures. As we mentioned before, production systems typically don't experience such a
failure pattern.

Pinheiro et al. \cite{pinheiro2007failure} analyzed the self-monitoring counters (SMART) of hard
disks searching for their correlations with disk failures. Based on their observations, failure
rates did increase sharply after the first year of power-on, but the pattern was irregular. This,
as they noted, was likely due to mixing of different disk models that are possibly having different
failure patterns. They proceeded to analyze the failure rate of disks categorized by a measure of
utilization, the metric being weekly average read/write bandwidth. This too shows great
irregularity and their conclusion did not favor correlation between failures and utilization.

Elerath et al. \cite{elerath2007enhanced} reported Weibull shape parameters of various disk
populations ranging from 0.9 (indicating slightly decreasing failure rates) to 1.48 (indicating
increasing failure rates), and scale parameters ranging from $7.5 \times 10^4$ hours (or 8.6 years)
to $4.6 \times 10^5$ hours (or 52 years). They observed that not all populations nicely fitted the
Weibull pattern for the entire duration of observation. Some of them fitted the distribution only
in a piece-wise manner, changing from a decreasing failure rate pattern (shape < 1) early in life
to an increasing failure rate pattern (shape > 1) after around 10k hours (1.14 years) of age. The
paper does not reveal any details about how the disk populations were deployed, managed and
observed.

Pasha et al. \cite{pasha2006empirical} presented an estimate of Weibull parameters of disk failure
distribution over age. The entirety of data they used contained sixteen disk failure events, using
which they fitted the Weibull cumulative distribution function. Aside from incurring a high
sampling error due to insufficient data points, their method itself is highly impractical to be
used correctly. Because, for correctness, all the disks taken into account (whose failures we have
witnessed) must have been under our observation from the beginning of their life till the end. Why?
Consider the following scenario, we are given 100 disks that are roughly one year old. We proceed
to deploy them and wait for years until they all failed and record their ages when they failed. If
we fit a Weibull CDF using this failure data, we'll get a distribution with extremely low failure
density during the first year of age, since all the disks were already older than one year in the
first place.

\section{Data Availability and Refinement}\label{sec:db-struct}

NetApp managed data centers runs a logging service which monitors software and hardware statuses
and sends a detailed summary at regular intervals. These logs are collected and stored in NetApp
warehouses in raw form. A part of these logs are processed and stored in relational databases so
as to make it easy to query and tranform. Perhaps due to resource constraints, only 6-12 months
worth of processed data is maintainde in relational form.

There were two tables of interest to us --- one containd counters and configuration details of
disks, and the other contained details of disk failure events. We limited our sample population to
have the same disk model to ensure that our sample population had uniform failure characteristics.
After performing sanity checks and filters, two tables with the following schemas were formed which
was used as a base for all further computations:

\begin{verbatim}
disk_logs: serial, datestamp, power-on hours, gigabytes transferred
           UNIQUE(serial, datestamp)

disk_failures: serial, datestamp
               UNIQUE(serial)
\end{verbatim}

The method we used (next section) for computing failure rates were such that as long as the
population being observed is sufficiently large, we could make do with data that was collected over
relatively short span of time (even just a few weeks).

\section{Failure Rate Computation}\label{sec:afr}

We want to compute the AFR of a disk separately for each year in deployment from the time of
manufacture. That is, we want to investigate the trend of AFR over age. To simplify, let's consider
only the disk under an year of age. The most straight-forward way to calculate this might be:

\[
    \text{AFR (age $<$ 1y)} = \frac{\text{Number of disk failures of age $<$ 1y}}
                                   {\text{Total number of disks of age $<$ 1y}}
\]

But for this calculation to be accurate, all disks that were observed within their first year of
lifetime should be observed throughout their entire first year of life (from the first day till the
365\textsuperscript{th} day)! To see the reason behind this, consider a population of $N$ disks
that were deployed from the time of their manufacture (age zero). After the first 6 months, half of
them were removed from deployment. This means that we are very likely to observe only half as many
failures during the later half of the year compared to the first half. Thus when calculating the
AFR during the first year of life, it would be incorrect to simply divide the number of failures by
$N$, the total number of disks observed.

Furthermore, it is very common in data centers to have disks move around or exchanged with vendors,
making older disks appear into our observation and working disks disappear from our observation. So
instead of trying to find a subset of disks that align with our desired window of observation, it
would be better to use a smarter approach which we can make use of all available data. A better
formula for AFR is given below:

\begin{equation}\label{eq:afr-1y}
    \text{AFR (age $<$ 1y)} = \frac{\text{Number of disk failures of age $<$ 1y}}
                                   {\text{Total disk-years observed of age $<$ 1y}}
\end{equation}

\begin{figure}[t]
    \centering
    \begin{subfigure}{0.48\textwidth}
        \includegraphics[width=\textwidth]{fr-step-1.eps}
        \caption{Counting failures}
    \end{subfigure}
    %add desired spacing between images, e. g. ~, \quad, \qquad, \hfill etc. 
    \begin{subfigure}{0.48\textwidth}
        \includegraphics[width=\textwidth]{fr-step-2.eps}
        \caption{Counting disk-years}
    \end{subfigure}

    %a blank line to force the subfigure onto a new line
    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{fr-step-3.eps}
        \caption{Failure rate}
    \end{subfigure}
    \caption[Failure rate computation steps]{Failure rate computation using data filtered on the
    most popular disk model, with nearly half a million disks under observation.}
    \label{fig:afr-steps}
\end{figure}

Here, a disk which was only \textit{observed} for half a year during its first year of life will
only contribute 0.5 to the ``total disk-years.'' An added advantage of using this method is that we
can actually calculate AFR using data collected over a span much smaller than a year (in wall clock
time). In fact, figure \ref{fig:afr-steps} was constructed using this method with just about four
weeks of disk logs from NetApp.

A generalized version of equation \ref{eq:afr-1y} was used to construct figure \ref{fig:afr-steps},
which gives the average AFR during the $n^\text{th}$ year of age as:

\begin{equation}\label{eq:afr}
    \text{AFR}_{\Delta t}(n) = \frac{C(n\Delta t) - C((n-1)\Delta t)}{V_{\Delta t}(n)}
\end{equation}

\begin{tabbing}
    \hspace{36pt}\=\hspace{1.5cm}\=\kill
    \>where \> $\Delta t = 1$\,yr, is the age-window of analysis,\\
    \>      \> $C(t)$ is the cumulative number of failures at age $t$,\\
    \>      \> $V_{\Delta t}(n)$ is the number of disk-years observed in the $n^\text{th}$ age-window,\\
    \>and   \> $n$ is a natural number.
\end{tabbing}

Obviously, we could apply this equation over smaller age-windows to obtain a finer distribution of
AFR. But as the age-window get smaller, we would need more data (or a larger observation period) so
that there are enough failures in each window. Figure \ref{fig:afr-smaller} was constructed using
data from about 14 weeks of data (of the same disk model).

\begin{figure}[h]
    \centering
    \begin{subfigure}{0.48\textwidth}
        \includegraphics[width=\textwidth]{fr-d6mo.eps}
        \caption{AFR using 6-month windows}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \includegraphics[width=\textwidth]{fr-d3mo.eps}
        \caption{AFR using 3-month windows}
    \end{subfigure}
    \caption{Varying the window size during AFR computation.} \label{fig:afr-smaller}
\end{figure}

The equation \ref{eq:afr} with small enough $\Delta t$ can be approximated as:

\begin{equation}\label{eq:afr-lim}
    \text{AFR}(t_n) = \frac{C(t_n + \Delta t) - C(t_{n})}{\Delta t \cdot N(t_n)}
\end{equation}

\begin{tabbing}
    \hspace{36pt}\=\hspace{1.5cm}\=\kill
    \>where \> $t_n = (n-1)\Delta t$ is the start of the $n^\text{th}$ age-window,\\
    \>and   \> $N(t)$ is the instantaneous count of disks under observation\\
    \>      \> with age $t$.
\end{tabbing}

Equation \ref{eq:afr-lim} in the limit $\Delta t \rightarrow 0$ becomes:
\begin{equation}\label{eq:afr-cont}
    \text{AFR}(t) = \frac{C'(t)}{N(t)}
\end{equation}
\begin{tabbing}
    \hspace{36pt}\=\hspace{1.5cm}\=\kill
    \>where \>$C'(t)$ is the derivative of the cumulative number of failures.
\end{tabbing}

$C(t)$ is a non-differentiable function since failures are instantaneous events. Therefore we apply
a technique known as Savitzky-Golay filter to simultaneously smooth and calculate its derivative.
Figure \ref{fig:afr-cont} shows this method in action.

\begin{figure}[t]
    \centering
    \begin{subfigure}{0.48\textwidth}
        \includegraphics[width=\textwidth]{fr-cont-pre.eps}
        \caption{$C'(t)$ and $N(t)$}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \includegraphics[width=\textwidth]{fr-cont.eps}
        \caption{Failure Rate}
    \end{subfigure}
    \caption{Computation of continuous failure rate}
    \label{fig:afr-cont}
\end{figure}

\begin{figure}[t]
    \footnotesize
    \begin{subfigure}{0.32\linewidth}
        \centering
        \begin{tabular}{c|cc}
            \hline
            \multirow{2}{*}{Serial no.}
                    & \multicolumn{2}{c}{Power-on hours}\\
                    & First & Last \\
            \hline
            ABC     & 100   & 700  \\
            EFG     & 2400  & 3400 \\
            PQR     & 2900  & 4500 \\
            XYZ     & 4000  & 5600 \\
        \end{tabular}
        \caption{Disk logs summary}
        \label{sfig:disk-summary}
    \end{subfigure}
    %\hfill
    \begin{subfigure}{0.32\linewidth}
        \centering
        \begin{tabular}{cc}
        \hline
        Power-on hours&$N(t)$\\
        \hline
        0    & 0  \\
        100  & 1  \\
        700  & 0  \\
        2400 & 1  \\
        2900 & 2  \\
        3400 & 1  \\
        4000 & 2  \\
        4500 & 1  \\
        5600 & 0  \\
        \end{tabular}
        \caption{Disks under observation}
    \end{subfigure}
    \begin{subfigure}{0.32\linewidth}
        \centering
        \includegraphics[width=\textwidth]{obs-constr.eps}
        \caption{Plot: $N(t)$ vs. $t$} %\label{fig:obs-constr}
    \end{subfigure}
    \caption{Computing number of disks under observation, Computing $N(t)$.}
    {
        \scriptsize \textbf{Note:} Power-on hours is used here since it is the standard unit in
        which logs are made. But we convert this into power-on years before using in equation
        \ref{eq:afr-cont}.
    }\label{fig:n_t-constr}
\end{figure}

\begin{figure}[t]
    \centering
    \noindent\begin{minipage}{.8\linewidth}
    \lstset{frame=tlrb,xleftmargin=\fboxsep}
    \begin{lstlisting}[language=sql]
WITH disk_summary AS (SELECT MIN(power_on_hrs) AS min_poh,
                             MAX(power_on_hrs) AS max_poh
                      FROM disk_logs
                      GROUP BY serial_no),
     bumps AS (SELECT min_poh AS poh, +1 AS bump
               FROM disk_summary
               UNION ALL
               SELECT max_poh AS poh, -1 AS bump
               FROM disk_summary),
     deltas AS (SELECT poh, SUM(bump) AS delta
                FROM bumps
                GROUP BY poh)
SELECT poh AS power_on_hours,
       SUM(delta) OVER (ORDER BY poh) AS N_t
FROM deltas;
    \end{lstlisting}
    \end{minipage}
    \caption{PostgreSql query to construct $N(t)$ from disk logs summary}\label{fig:sql-nt}
\end{figure}

For illustration purposes, figure \ref{fig:n_t-constr} shows a small and simplistic example of how
$N(t)$ can be constructed from disk logs; and figure \ref{fig:sql-nt} shows an example SQL query to
construct $N(t)$ from plain disk logs (discussed in section \ref{sec:db-struct}), where
construction of disk logs summary, as in table \ref{sfig:disk-summary}, is an intermediate step.

\section{Fitting Theoretical Distributions}

\begin{figure}[h]
    \centering
    \begin{subfigure}{0.48\textwidth}
        \includegraphics[width=\textwidth]{dist-fit.eps}
        \caption{Normal plot}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \includegraphics[width=\textwidth]{dist-lfit.eps}
        \caption{Semi-log plot}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \includegraphics[width=\textwidth]{dist-llfit.eps}
        \caption{Log-log plot}
    \end{subfigure}
    \caption{Fitting theoretical distributions}
    \label{fig:afr-fit}
\end{figure}

\begin{figure}[t]
    \centering
    \begin{subfigure}{\textwidth}
        \centering
        \begin{tabular}{cccc}
            Distribution & Parameters & Mean (years) & Fitting loss \\
            \hline
            Weibull      & $k$ = 2.8399, $\lambda$ = 8.5619  & 7.6281 & 0.2616 \\
            Gamma        & $k$ = 4.7792, $\theta$ = 1.7497   & 8.3622 & 0.3266 \\
            Log-normal   & $\sigma$ = 0.5283, $\mu$ = 2.0835 & 9.2355 & 0.4005 \\
        \end{tabular}
        \caption{Parameters and the goodness of fit}
        \label{sfig:dist-params}
    \end{subfigure}

    \begin{subfigure}{0.5\textwidth}
        \includegraphics[width=\textwidth]{dist-pdfs.eps}
        \caption{Probability Density Functions}
        \label{sfig:dist-pdfs}
    \end{subfigure}
    \caption{Estimated distribution parameters}
\end{figure}

Since we directly obtained the empirical failure rate curve over the lifetime of hard disks, we can
directly fit the hazard rate functions of theoretical distributions and estimate its parameters.
The hazard rate of a probability distribution is given by

\[
    h_T(t) = \frac{f_T(t)}{S_T(t)}
\]
\begin{tabbing}
    \hspace{36pt}\=\hspace{1.5cm}\=\kill
    \>where \> $f_T(t)$ is the probability density function\\
    \>and   \> $S_T(t) = \Pr(T > t) = \int_t^\infty f_T(t) dt$ is the survival function
\end{tabbing}

When fitting a hazard rate function to our empirical failure rate curve, we have to take into
account the significance of each point on our empirical curve. A good measure of significance may
be obtained from the observed population curve, $N(t)$.

We fitted the hazard rate functions of Weibull, Gamma and Log-normal distributions. Figure
\ref{fig:afr-fit} shows all three fitted hazard rate curves, as well as the empirical values (as a
gradient-colored line). The lightness of a point in the empirical curve denotes the weight of that
point --- darker colors (towards purple) are more significant and lighter points (towards yellow)
are less significant.

The fitting was done by using weighted least-squares method where the weight of each point was
(proportional to) the value of $N(t)$ at that point. That is, by minimizing the loss function $g$,
defined as:
\[
    \argmin_{\phi_1, \phi_2, ...} g(\phi_1, \phi_2, ...)
\]

\[
    g(\phi_1, \phi_2, ...) = \sum_t N(t) \times ( \hat{h}(t) - h_T(t) )^2 \\
\]
\begin{tabbing}
    \hspace{36pt}\=\hspace{1.5cm}\=\kill
    \>where \> $\hat{h}(t)$ is the empirical failure rate values,\\
    \>      \> $h_T(t)$ is the hazard rate function over $T$,\\
    \>and   \> $T \sim DIST(\phi_1, \phi_2, \cdots)$ where \= $\phi_1, \phi_2, \cdots$ are the
                                                              parameters of the \\
    \>      \>                                             \> probability distribution, $DIST$.
\end{tabbing}

The probability density functions (PDF) of the three fitted distributions are given below for
reference:
\begin{align*}
    &{\rm Weibull}(k, \lambda): &f(x) &= \frac{k}{\lambda}\left(\frac{x}{\lambda}\right)^{k-1}
                                             e^{-(x/\lambda)^k} & x \ge 0\\
    &{\rm Gamma}(k, \theta): &f(x) &= \frac{1}{\Gamma(k)\theta^k} x^{k\,-\,1}
                                          e^{-{\frac{x}{\theta}}} & x \ge 0\\
    &{\rm Lognormal}(\sigma, \mu): &f(x) &= \frac{1}{x\sigma \sqrt{2\pi}}
                                                 e^{-{\frac{(\ln x-\mu )^2}{2\sigma^2}}} & x \ge 0
\end{align*}

The distribution parameters, mean of the fitted distributions and the value of the loss function
after fitting are given in figure \ref{sfig:dist-params}, and the resulting PDFs are shown in
figure \ref{sfig:dist-pdfs}. Note that the parameters were estimated using the failure rate over
power-on \textit{years}, not power-on hours. This has no effect on the shape parameters of Weibull
($k$), Gamma ($k$) or Log-normal ($\sigma$) distributions, and affects only their scale parameters
($\lambda$, $\theta$ or $\mu$). The distribution with the lowest fitting error was found to be the
Weibull distribution with a shape parameter of 2.84, and mean time to failure of 7.63 years or
66.8k hours, far less than 1M hours (as typically specified in data sheets provided by vendors).

\section{Utilization-based Analysis}

Functional/IO Duty Cycle is a normalized measure of utilization found in the literature. It is
basically the fraction of time a device is being fully utilized. But since it is difficult to
define ``full-utilization'' for a hard disk, we used data transfer rate instead as the measure of
utilization. For the sake of brevity, we also use the term ``load'' to indicate ``data transfer
rate''. We used two different approaches to analyze the effects of disk utilization on failure
rates, one to detect possible long-term correlations, and the other for short-term correlations.

\subsection{Long-term correlation}

\begin{figure}[h]
    \centering
    \begin{subfigure}{0.48\textwidth}
        \includegraphics[width=\textwidth]{twin-cors.eps}
        \caption{Normal plot}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \includegraphics[width=\textwidth]{twin-cors-l.eps}
        \caption{Semi-log plot}
    \end{subfigure}
    \caption{Long-term correlation between utilization and failure rate}
    \label{fig:wl-cors}
\end{figure}

Here, we ask the question whether disk failure rate is affected by the average load over the span
of observation (equation \ref{eq:lavg-load} where ``last'' and ``first'' respectively indicate
``last observed value'' and ``first observed value'' from the logs).

\begin{equation}\label{eq:lavg-load}
    \text{Avg load} = \frac{\text{last(data transferred) $-$ first(data transferred)}}
                           {\text{last(power-on hours) $-$ first(power-on hours)}}
\end{equation}

We first categorized the disks into two buckets based on the average load. The first bucket
contained those which experienced between 0 and 1.6 GB/hr, and the other bucket between 4 and 16
GB/hr (the rest were discarded). Then we used the method we described in the previous section for
each bucket to calculate the AFR over the disk age.

The result is given in figure \ref{fig:wl-cors}. The failure rates does not seem to be affected by
the load. This might be because the difference in data transfer rates were rather small. (Note that
a typical enterprise hard disk can provide a throughput of more than 540 GB/hr or 150 MB/s. But the
number of disks which faced an average transfer rate higher than 16 GB/hr were too low.)

\subsection{Short-term correlation}\label{ssec:short}

\begin{figure}[h]
    \centering
    \begin{subfigure}{0.48\textwidth}
        \includegraphics[width=\textwidth]{twin-fine.eps}
        \caption{Normal plot}
    \end{subfigure}
    \begin{subfigure}{0.48\textwidth}
        \includegraphics[width=\textwidth]{twin-fine-l.eps}
        \caption{Semi-log plot}
    \end{subfigure}
    \caption{Short-term correlation between utilization and failure rate}
    \label{fig:wl-fine}
\end{figure}

Here, we look for correlation between failure rate and ``instantaneous'' data transfer rate. More
specifically, whether disks are more likely to fail during the weeks of high load --- possibly
pointing to a short-term/transient effect. For this, instead of treating a disk as a single entity,
we treat each weekly summary of a disk as a separate entity. The logs were pre-processed to make
each weekly summary contain the starting and ending values of power-on hours and total data
transferred in that week. From these values, we can directly calculate the average load in that
week, and then categorize the logs and failure events based on the average load during that week.
Figure \ref{fig:obs-wkly-steps} depicts the various steps involved in calculating $N(t)$ (from
equation \ref{eq:afr-lim} and \ref{eq:afr-cont}) for both buckets when there is only a single disk.

\begin{figure}[h]
    \footnotesize
    \begin{subfigure}{\linewidth}
        \centering
        \begin{tabular}{cc|ccc}
            \hline
            \multirow{2}{*}{Serial no.}
                    & \multirow{2}{*}{Week no.}
                        & \multicolumn{2}{c}{Power-on hours}
                                       & Avg. Load \\
                    &   & First & Last & (GB/hr) \\
            \hline
            ABC     & 1 & 400   & 560  & 0.4  \\
            ABC     & 2 & 560   & 730  & 9.2  \\
            ABC     & 3 & 730   & 900  & 3.6  \\
            ABC     & 4 & 900   & 1060 & 8.3  \\
            ABC     & 5 & 1060  & 1190 & 1.0  \\
        \end{tabular}
        \caption{Disk logs (weekly summaries)}
        \label{sfig:disk-logs-wkly}
    \end{subfigure}
    \vspace{16pt}
    \begin{subfigure}{0.31\linewidth}
        \centering
        \begin{tabular}{cc}
        \hline
        Power-on hours & Count \\
        \hline
        0              & 0     \\
        400            & 1     \\
        560            & 0     \\
        1060           & 1     \\
        1190           & 0     \\
        \end{tabular}
        \caption{Disks under observation for 0-1.6 GB/hr bucket}
        \label{sfig:obs-pop-wkly-lo}
    \end{subfigure}
    \hspace{0.02\linewidth}
    \begin{subfigure}{0.3\linewidth}
        \centering
        \begin{tabular}{cc}
        \hline
        Power-on hours & Count \\
        \hline
        0              & 0     \\
        560            & 1     \\
        730            & 0     \\
        900            & 1     \\
        1060           & 0     \\
        \end{tabular}
        \caption{Disks under observation for 4-16 GB/hr bucket}
        \label{sfig:obs-pop-wkly-hi}
    \end{subfigure}
    \hspace{0.03\linewidth}
    \begin{subfigure}{0.3\linewidth}
        \centering
        \includegraphics[width=\textwidth]{obs-constr-wkly.eps}
        \caption{Disks under observation plotted.}
    \end{subfigure}
    \captionsetup{justification=centering}
    \caption[Computing $N(t)$, categorized into utilization-based buckets.]{Computing $N(t)$,
    categorized into utilization-based buckets. A single disk may contribute to both buckets.
    \scriptsize \textbf{Note:} The entry for week 3 is not included in either
    bucket.}\label{fig:obs-wkly-steps}
\end{figure}

Figure \ref{fig:wl-fine} shows significant positive correlation between disk failure rate and disk
load. The difference in failure rates were, for the most part, more than a factor of 2. But high
correlation doesn't necessarily mean causation. We suspect that a higher load on the disk may be
directly related to failure detection or inspection mechanisms, and not necessarily a cause of
actual failures. To understand this issue, we need to take a deeper look at how a disk ``fails.''
In most production deployments, the monitoring/management softwares tend to prematurely consider a
disk to have failed, based on certain criteria that are indicative of physical degradation. One of
these criteria could be to check whether the number of sector failures crossed a certain threshold.
Sector failures cannot be detected unless data is read from (or written to?) a failed sector.
Therefore, detection of sector failures could be positively correlated with the rate of reads, and
consequently detection of ``disk failures'' (esp. when a scrub is going on). Thus the apparent
difference in failure rate seen in figure \ref{fig:wl-fine} does not necessarily indicate a
cause-effect relationship between utilization and failures.
