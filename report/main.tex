\documentclass[12pt]{report}

\usepackage[a4paper, top=30mm, bottom=22mm, left=25mm, right=25mm]{geometry}
\linespread{1.15}
\usepackage{parskip} % for spacing between paras

\usepackage{mathtools}
\usepackage{graphicx}
\usepackage{wrapfig}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{multirow}
\usepackage{fancyhdr}
\usepackage{cite}
\usepackage{color}
\definecolor{myblue}{rgb}{0.1,0.2,0.7}
\usepackage{listings}
\lstset{basicstyle=\scriptsize\ttfamily,keywordstyle=\bfseries\color{myblue}}
\usepackage{hyperref}
\renewcommand\UrlFont{\ttfamily\footnotesize}
%\usepackage[hyphenbreaks]{breakurl} %only needed when dealing with dvips
%\patchcmd{\chapter}{plain}{empty}{}{}
\usepackage[toc,page]{appendix}
\usepackage{datetime}
\usepackage{varwidth}

\renewcommand{\theequation}{\thechapter.\alph{equation}}
\renewcommand{\thesubfigure}{\roman{subfigure}}
\renewcommand{\thesubtable}{\roman{subtable}}

\pagestyle{fancy}
\fancyhf{}
\fancyhead[RE,LO]{\itshape\nouppercase{\leftmark}}
\fancyhead[LE,RO]{\thepage}

\renewcommand{\chaptermark}[1]{%
    \markboth{\thechapter.\ #1}{}}

\newcommand{\Prcond}[2]{\Pr\left[\, #1 \,\middle\vert\, #2 \,\right]}
\DeclareMathOperator*{\argmin}{arg\,min}
\newcommand{\Iint}[4]{\int\limits_{#1}^{#2} \hspace{-4pt}\int\limits_{#3}^{#4}}
\newcommand\numberthis{\addtocounter{equation}{1}\tag{\theequation}}

\captionsetup[figure]{labelfont={bf,small},textfont=small}
\captionsetup[table]{labelfont={bf,small},textfont=small}

\graphicspath{{assets/}}

\hypersetup{pdfauthor={John C F},%
            pdftitle={Reliability Analysis of Storage Systems},%
            pdfproducer={LaTeX},%
            pdfcreator={pdfLaTeX}
}

\renewcommand\bibname{References}

\title{Empirical and Theoretical Reliability Analysis of Storage Systems}
\author{John C F}

\begin{document}

\pagenumbering{gobble}
\begin{titlepage}
    \begin{center}
        \large
        ~\par \vspace{\stretch{0.3}}
        {\LARGE \bfseries Empirical and Theoretical \\
         Reliability Analysis of \\Storage Systems\par}

        \vspace{\stretch{0.4}}
        {\it Dissertation submitted in partial fulfillment of\\
        the requirements for the degree of}\\[0.75\baselineskip]
        Master of Technology\par

        \vspace{\stretch{0.2}}
        {\it by}\par
        \vspace{\stretch{0.2}}
        \textbf{John C F}\par
        \vspace{.25\baselineskip}
        {\normalsize 14305R006}

        \vspace{\stretch{0.3}}
        {\it Under the guidance of}\par
        \vspace{.25\baselineskip}
        \textbf{Prof. Varsha Apte} \par

        \vspace{\stretch{0.3}}
        \includegraphics[width=8em]{iitb-logo.eps}

        \vspace{\stretch{0.1}}
        Department of Computer Science \& Engineering\\
        Indian Institute of Technology Bombay \\
        India -- 400076 
        \vspace{\stretch{0.1}}

        {\normalsize 2017 \par}
        %\vspace{\stretch{0.2}}
    \end{center}
\end{titlepage}

\renewcommand{\thepage}{\roman{page}}

% ==================== Approval Sheet ====================
\clearpage
\thispagestyle{empty}
\begin{figure}
  \includegraphics[width=\textwidth]{00-approval.png}
\end{figure}
\vfill

%% ==================== Declaration ====================
\clearpage
\thispagestyle{plain}
\begin{figure}
  \includegraphics[width=\textwidth]{00-declaration.png}
\end{figure}
\vfill

% ==================== Acknowledgements ====================
\clearpage
\thispagestyle{plain}
{\null\vfill}
\begin{center}
    \bfseries Acknowledgements
\end{center}

I would like to express my sincere thanks to my guide \textbf{Prof. Varsha Apte} for providing me
with directions whenever things got me confused. Her clarity of vision has always helped in
defining and investigating the path forward.

I extend my thanks to \textbf{NetApp} for funding the project, and especially to \textbf{Mr.~Vipul
Mathur}, \textbf{Mr.~Atish Kathpal} and \textbf{Mr.~Ajay Bhakshi} from NetApp for providing us with
insider insights and access to on-site data for analysis.

I am greatly thankful to Indian Institute of Bombay, and the Department of Computer Science and
Engineering for providing me this opportunity and supporting this work. I cannot overstate the
immense knowledge I gained from working with the professors of this wonderful Institution.

Finally, I cannot express enough gratitude towards my parents, family members and friends for their
support and encouragement throughout my life.

\vspace{0.5\baselineskip}
\begin{flushright}
    \bf John C F \hspace{8pt}
\end{flushright}
{\vfill\null}

% ==================== Abstract ====================
\clearpage
\thispagestyle{plain}
{\null\vfill}
\begin{center}
    \bfseries Abstract
\end{center}
Estimating data reliability and providing high reliability guarantees is critical in most fields
today. But a generalized and systematic way to estimate reliability of a storage system does not
exist as of now.

One way to estimate the reliability of a system is to deploy the system and monitor it. But this is
usually too expensive and time-consuming to do in practice, especially when the size of the system
is large. Therefore a better approach would be to make a mathematical description of the system
based on design and the known behavior of its components, and use it to analyze the overall system
behavior computationally.

Our main goal was to develop a useful tool that engineers can use to analyze the reliability of a
storage system. This goal got translated to two major concerns that we tried to address: (1) to
develop a more accurate failure model for storage devices, specifically, hard disks; (2) to develop
an efficient and accurate method for analyzing empirical failure data of hard disks. In this
report, we present our experience gathered along the journey in pursuit of these goals.
{\vfill\null}

% ==================== Table of Contents ====================
\tableofcontents

% ==================== List of Figures ====================
\listoffigures

\clearpage
\setcounter{page}{1}
\renewcommand{\thepage}{\arabic{page}}

\include{intro}
\include{bg}
\include{theory-1}
\include{theory-2}
\include{empirical}
\include{summary}

\begin{appendices}
    \include{app-tool}
\end{appendices}

\addcontentsline{toc}{section}{References}
\bibliographystyle{ieeetr}
\bibliography{refs}

\end{document}
