\chapter{Efficient Reliability Analysis using Simulation}\label{ch:imp-samp}

In this chapter, we will discuss how simulation could be implemented efficiently enough to obtain
results within practical number of simulation runs. We used a method called Importance Sampling to
efficiently simulate these rare data loss events even in highly redundant storage systems.

For a detailed explanation of the theory behind importance sampling, and to fill possible gaps in
our explanation, please refer to \cite{heidelberger1995fast} survey paper.

\section{Importance Sampling}

Importance Sampling, intuitively, is a technique for quickly discovering failure paths that can
lead to total system failure (in the face of redundancies), and then estimate the probability of
actually taking that path. The challenge lies in the discovery process itself (and not on the
probability estimation process). If paths that are more likely to be taken by the system in reality
are not discovered by the method, then the reliability might be (even severely) over-estimated.

The probability that a random variable $X$, having probability density $p(x)$, is in some set $A$
is given by:

\begin{equation}\label{pr-XinA}
    \gamma = \int\limits_{-\infty}^{\infty} 1_{x \in A}~p(x) dx = E_p [ 1_{x \in A} ]
\end{equation}
\begin{tabbing}
    \hspace{36pt}\=\hspace{1.5cm}\=\kill
    \>where \> $\gamma$ is the probability of the (rare) event of interest, \\
    \>and   \> $1_{x \in A}$ the indicator function for membership of $x$ in $A$.
\end{tabbing}

To estimate by simulation, we draw $N$ samples $(X_1...X_N)$ and:
\[
    \widehat{\gamma}_N = \frac{1}{N} \sum_{n=1}^{N} I_n
\]
\begin{tabbing}
    \hspace{36pt}\=\hspace{1.5cm}\=\kill
    \>where \> $I_n = 1_{X_n \in A}$
\end{tabbing}

We may estimate its variance, and find out the sample size ($N$) required to bound the relative
error of the estimate. It turns out that $N$ should be proportional to $1/\gamma$. So, for rare
events with $\gamma < 10^{-6}$, standard simulation becomes infeasible.

If we divide and multiply the integral in equation \ref{pr-XinA} by another probability density
function $p'(x)$, we get:
\begin{equation}\label{pr-XinA-IS}
    \gamma = \int\limits_{-\infty}^{\infty} 1_{x \in A} \frac{p(x)}{p'(x)} p'(x) dx = E_{p'} [ 1_{x \in A}L(x)]
\end{equation}
\begin{tabbing}
    \hspace{36pt}\=\hspace{1.5cm}\=\kill
    \>where \> $L(x) = \frac{p(x)}{p'(x)}$ is called the likelihood ratio.
\end{tabbing}

Equation \ref{pr-XinA-IS} is valid for any density $p'$, provided $p'(x)>0$ for all $x \in A$ if
$p(x)>0$; and the density $p'$ is called the importance sampling density.

This equation suggests the following scheme (which is called importance sampling): draw $N$ samples
$X_1, ..., X_N$ using the density $p'$. Then an unbiased estimate of $\gamma$ is given by:
\[
    \widehat{\gamma}_{N, p'} = \frac{1}{N} \sum_{n=1}^{N} I_n \times L(X_n)
\]
Because, from \ref{pr-XinA-IS}, $E_{p'}[I_n L(X_n)] = \gamma$.

Our job is to select an optimal (and practical) density $p'$ such that the variance of $I_n L(X_n)$
is bounded (so as to bound the relative error of the estimate with a small $N$ independent of
$\gamma$). It turns out that we should pick $p'$ such that $p'(x)$ is large on $A$; i.e., one which
makes the event $A$ more likely to occur.

This becomes a non-trivial task when the system we are simulating becomes complex, and the knobs we
control (such as the failure distribution of a component) does not directly control the state of
the system (such as whether the system has failed or not). For dealing with this, some standard
techniques are available in the literature.

\section{Uniformized Balanced Failure Biasing}\label{sec:ubfb}

For a Non-Homogeneous Poisson Process (NHPP) with intensity rate $\lambda(t)$, if there exists a
finite constant $\beta$ such that $\lambda(t) \le \beta$ for all $t \ge 0$, then points in NHPP can
be simulated as follows. Draw points ${t_i,i \ge 1}$ from ordinary Poisson process with constant
rate $\beta$. Accept a point $t_i$ as a point in NHPP with probability $\lambda(t_i)/\beta$,
otherwise reject it as a ``pseudo event". This technique is called \textbf{Uniformization}.

To incorporate importance sampling, we will simulate a different Poisson process with (higher)
intensity rate $\lambda'(t)$ (where $\beta \ge \lambda'(t)$ for all $t$). If $N(t)$ denote the
number of events (both real and pseudo) generated in time $t$, then the net likelihood ratio is
given by:
\[
    L(t) = \prod_{i=1}^{N(t)} L_i
\]
\begin{tabbing}
    \hspace{36pt}\=\hspace{1.5cm}\=\kill
    \>where \> $L_i$ is the likelihood factor of the $i$\textsuperscript{th} event (at $t_i$), given by:
\end{tabbing}
\[
    L_i =
    \begin{dcases}
        \frac{\lambda(t_i)}{\lambda'(t_i)}
            & \textrm{if $i$\textsuperscript{th} event was a real event}\\
        \frac{1 - \lambda(t_i)/\beta}{1 - \lambda'(t_i)/\beta}
            & \textrm{if $i$\textsuperscript{th} event was a pseudo event}
    \end{dcases}
\]

\section{Storage System Simulation}\label{sec:stor-is-theory}

Here, we briefly discuss how failures in a storage system can be simulated using importance
sampling, and how the likelihood ratio of a simulation run is computed. We will discuss the actual
algorithms involved later in chapter 5.

We choose $\beta$ such that it is greater than repair rate of disks (assuming that the total
failure rate of all active hard disks never exceeds $\beta$). We also choose a constant, $P_{fb}$,
called failure biasing probability such that $0.5 < P_{fb} < 0.9$. The exact value of this constant
does not have much consequence, and can be tuned together with $\beta$ so as to minimize variance.

As discussed in the previous section, we generate events with events with rate $\beta$, and
considers it a ``failure event'' with probability $P_{fb}$, otherwise mark it as a ``pseudo
event''. This is equivalent to simulating a Poisson process with rate, $\lambda'(t) = P_{fb}
\times \beta$. If it is a failure event, pick an active disk uniformly at random and mark it
``failed''. Therefore, the effective failure rate of a single disk (say, disk $j$) would be:
\[
    \lambda'_j(t) = \frac{P_{fb} \times \beta}{|\text{\sc active}|}
\]
\begin{tabbing}
    \hspace{36pt}\=\hspace{1.5cm}\=\kill
    \>where \> $|\text{\sc active}|$ denotes the number of active disks in the system.
\end{tabbing}

And the likelihood factor of the $n$\textsuperscript{th} event is given by:
\begin{equation}\label{eq:ubfb-lr}
    L_n =
    \begin{dcases}
        \frac{\lambda_j(t_n)/\beta}{P_{fb}/|\text{\sc active}|}
            & \textrm{if $n$\textsuperscript{th} event was a failure event of disk $j$}\\
        \frac{1 - \lambda(t_n)/\beta}{1 - P_{fb}}
            & \textrm{if $n$\textsuperscript{th} event was a pseudo event}
    \end{dcases}
\end{equation}
\begin{tabbing}
    \hspace{36pt}\=\hspace{1.5cm}\=\kill
    \>where \> $\lambda_j(t_n)$ denotes the actual failure rate of disk $j$, \\
    \>and   \> $\lambda(t_n) = \sum_j \lambda_j(t_n)$ denotes the total failure rate
\end{tabbing}

\section{Implementation}

An implementation of this technique has been published by Greenan \cite{greenan2009hfrs}, which is
used to analyze the reliability of various coding techniques (such as MDS), and the associated tool
named HFRS is publicly available. Because it was written in Python, making it was hard to extend
and debug, we reimplemented it in a strongly typed language with a powerful and expressible type
system. We later extended it with the theory discussed in chapter \ref{ch:bivar}. The algorithm and
data structure design used in our implementation is discussed in detail in Appendix \ref{app:tool}.
Our implementation is publicly available at \url{https://gitlab.com/johncf/stosim}.
