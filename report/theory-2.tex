\chapter{Modeling Utilization in Reliability Analysis}\label{ch:bivar}

The model we discussed until now only took disk age into consideration, but this seemed limiting
since disk failures have shown correlation with their utilization factors \cite{cole2000seagate}.
Thus we searched for ways to incorporate disk utilization into our model. Our primary hypothesis
based on what little evidence was published in the literature was that disk failure rate was
affected by the rate at which data is transferred. We found that using a bivariate distribution
instead of a univariate distribution for predicting failures captures such a behavior, which we
present in this chapter.

The theory presented here is orthogonal to the Importance Sampling theory discussed in the previous
chapter. However, at the end of section \ref{ssec:bivar-exp}, we will briefly discuss how
Importance Sampling can be extended using this theory.

\section{Background}

We used a bivariate probability distribution which has a time axis representing age at which disks
fail and an I/O axis representing the amount of data transferred before disks fail. Here, we'll
discuss the consequences of having 2 axes affecting failures, and specifically detail how disk
failures can be simulated.

In this model, each disk ``degrades" with both time and I/O. So we need to have a model of I/O
within the system. We'll be using the following simplistic model of I/O:

\begin{itemize}
    \item The system faces a constant external read requests, which needs to be served using data
        stored on the component disks. This external load is measured in terms of outgoing data
        rate.
    \item Given an external load on the system and the status of each disk, it is possible to
        deterministically figure out the spread of I/O load over each component disks.
    \item The status of a disk simply denotes whether it is up and running, or has failed and being
        repaired.
    %\item A disk being repaired cannot fail, and once repaired will be treated as brand new.
\end{itemize}

Let's take a look at an example of how this model will describe a RAID-6 system having 5 disks with
distributed parity. This system can tolerate a maximum of 2 failures. Let's assume that the
external load on the system is 100 MB/s reads. And that when a repair job is going on, all active
disks will experience an additional load of 10 MB/s due to the reconstruction job. Thus, if all
disks in the system are up, the system load will be evenly distributed among the disks which gives
20 MB/s for each disk. When a disk fails, the external load has to be distributed among the
remaining 4 disks, and the reconstruction job will exert an additional load on each of them; with
the net load on each disk amounting to 35 MB/s. If one more fails, the disks will face 43.3 MB/s
load each.

Now, to simulate disk failures in this setup, we can no longer use the same method of sampling a
point using the failure distribution and simply fast-forward the simulation until the nearest among
the sampled points. This is because, when you sample a point from a bivariate distribution, you get
a pair of numbers representing the 2D coordinate in the Age-I/O plane. But, as previously seen,
disks are driven according to the system state, and the path taken by a disk in the Age-I/O plane
may never pass through the sampled point. The next obvious approach is to use a rectangular bound
with that sampled point as the corner. But this would result in a higher failure rate than what is
described by the distribution.

Therefore we decided to use the survival function as an estimate of disk health. The disks are
pre-assigned a minimum-survival-value by sampling from a uniform distribution over the interval
$[\,0,1)$. All disks starts with a survival value of 1. As time passes, their survival values
decreases, possibly at different rates depending on the load on each disk. When the survival value
of a disk falls below the minimum-survival-value assigned to it, it is considered to have failed.
During simulation, this can be sped up by deriving an expression for inverse-survival function
along linear paths. An example of how this can be done in practice is discussed in section
\ref{ssec:bivar-exp}.

\section{Bivariate Distribution}

We use random variables $T$ and $X$, where $T$ denote the age at which the disk fails, and $X$
denote the total data transferred before the disk fails. Then their joint probability density
function, $f_{T,X}$ satisfies the following equation:
\begin{equation}\label{eq:bivar-constr}
    \Iint{0}{\infty}{0}{\infty} f_{T,X}(t, x) \;dt \;dx = 1
\end{equation}

Since we are trying to find the failure dependence on the load faced by the disk, we will introduce
another variable $W = X/T$, which represents the average load faced along its entire lifetime. (It
should be noted that previously, in stage 1, the mathematical model was not entirely correct.)

In equation \ref{eq:bivar-constr}, we can substitute for $x \in \mathrm{dom}(X)$ with equivalent
expressions in $w \in \mathrm{dom}(W)$ to compute the density function $f_{T,W}(t, w)$. We know:
\begin{align*}
    x &= wt \\
    dx &= wdt + tdw \\
    x = 0 &\Rightarrow w = 0 \\
    x \rightarrow \infty &\Rightarrow w \rightarrow \infty \\
\end{align*}

Substituting all of these in equation \ref{eq:bivar-constr}, we get
\begin{equation*}
    \Iint{0}{\infty}{0}{\infty} f_{T, X}(t, wt) \;dt (wdt + tdw) = 1
\end{equation*}

Noting that $(dt)^2 = 0$, we get
\begin{equation*}
    \Iint{0}{\infty}{0}{\infty} t \;f_{T, X}(t, wt) \;dt \;dw = 1
\end{equation*}

Therefore, we have
\begin{equation}\label{eq:bivar-tw}
    f_{T,W}(t, w) = t \;f_{T,X}(t, wt)
\end{equation}

From this equation, we can calculate both $f_W(w)$ and $f_{T|W}(t|w)$ as follows:
\begin{align}
    f_W(w) &= \int\limits_0^\infty f_{T,W}(t, w) \;dt \nonumber\\
           &= \int\limits_0^\infty t\;f_{T,X}(t, wt)\;dt \label{eq:marginal-w}\\
    f_{T|W}(t|w) &= \frac{f_{T,W}(t,w)}{f_W(w)} \label{eq:bivar-cond}
\end{align}

Equation \ref{eq:bivar-cond} gives the probability density function over time on the condition that
the load on the disk stays constant throughout its life. %We'll use this equation for bivariate
%exponential distribution and arrive at some interesting expressions.

\subsection{Survival Function} % FIXME continuity?

In the univariate case, when failures are affected only by the age of disks, the survival function represents the probability that a disk survives till a certain age. That is,
\begin{align*}
    S_T(t) &= \Pr (T > t) \\
           &= \int\limits_{t}^\infty f_{T}(t) \;dt \\
           &= 1 - F_T(t)
\end{align*}
where $S_T(t)$ is the survival function and $F_T(t)$ is the cumulative distribution function (CDF).
This definition can be generalized to the bivariate case as follows:
\begin{align*}
    S_{T,X}(t, x) &= \Pr(T > t, X > x) \\
                  &= \Iint{t}{\infty}{x}{\infty} f_{T,X}(t, x) \;dx \;dt
\end{align*}

Note that $S_{T,X}(t, x) \ne 1 - F_{T,X}(t, x)$ unlike the univariate case. This can be easily seen
by the noting that the subdomain $T > t, X > x$ is not the complement of $T \le t, X \le x$.

As we mentioned in the previous section, the intuition behind choosing $S_{T,X}(t, x)$ as the
measure of health of disks during simulation is that $S_{T,X}(t,x)$ gives the probability mass of
the subdomain $T > t, X > x$ which is the subdomain the disk can grow into, since both variables
are non-decreasing. As such, $S$ seemed to be a measure of future prospects, and assigning a limit
to $S$ will effectively and fairly limit how long each disk can operate.

\subsection{Hazard Rate}

The conditional probability that a device fails in the next instant given that it survived for time
$t$, is given by:
\begin{align*}
    \Prcond{T \le t + \Delta t}{T > t} &= \frac{\Pr(t < T \le t + \Delta t)}{\Pr(T > t)} \\
                                       &\approx \frac{f_T(t) \Delta t}{S_T(t)}
                                                               \tag{for small $\Delta t$}
\end{align*}

And the Hazard Rate (a.k.a failure rate or force of mortality) is defined as:
\begin{align*}
    h_T(t) &= \lim_{\Delta t \rightarrow 0} \frac{\Prcond{T \le t + \Delta t}{T > t}}{\Delta t} \\
           &= \frac{f_T(t)}{S_T(t)}
\end{align*}

Therefore, $h_T(t)$ is sometimes called the ``conditional density of failures'' (conditioned on
surviving until $t$), in contrast to $f_T(t)$ being the ``unconditional density of failures''.
Hazard rate is useful to understand the pattern of failures from a distribution.

\subsection{Independent and Exponential}\label{ssec:bivar-exp}

For simplicity, let's assume that the variables $T$ and $X$ are independent and are both
exponentially distributed.

\begin{align}
    T &\sim EXP(\lambda) \nonumber\\
    X &\sim EXP(\theta) \nonumber\\
    f_{T, X}(t, x) &= \lambda\theta e^{- \lambda t - \theta x} \label{eq:bivar-exp}
\end{align}

\subsubsection{Fast-Forwarding till Failure}

The following demonstrates how to calculate the time to failure ($\Delta t$) when the load on the
disk is known. This method can be used to fast-forward simulation time until the first failure.
Given that currently the disk is of age $t_0$ and has served $x_0$ I/O in total, we can calculate
the age ($t$) and total I/O ($x$) when it would fail as follows:
\begin{align*}
    t &= t_0 + \Delta t \\
    x &= x_0 + w \Delta t \\
    S_{X, T}(x, t) &= e^{-\lambda t - \theta x} \\
    S_{\rm min} &= e^{-\lambda (t_0 + \Delta t) - \theta (x_0 + w\Delta t)} \\
    \intertext{We can solve for $\Delta t$ to get the time-offset to failure,}
    \Delta t &= \frac{-\log S_{\rm min} - (\lambda t_0 + \theta x_0)}{\lambda + w\theta}
    \numberthis\label{eq:fff}
\end{align*}

\subsubsection{Conditional Hazard Rate}

Here we will show that even our simple bivariate exponential model exhibit varying failure rate
depending on the load on the disk. To that end, let us first derive the conditional distribution
w.r.t time given a particular load on the disk. This can be done by substituting equation
\ref{eq:bivar-exp} in equations \ref{eq:bivar-tw}, \ref{eq:marginal-w} and \ref{eq:bivar-cond}:
\begin{align*}
    f_{T,W}(t, w) &= \lambda\theta \;t \;e^{- \lambda t - \theta wt} \\
    f_W(w) &= \int\limits_0^\infty t\;\lambda\theta e^{- \lambda t - \theta wt} \;dt \\
           &= \frac{\lambda\theta}{(\lambda + \theta w)^2} \\
    f_{T|W}(t|w) &= \frac{f_{T,W}(t,w)}{f_W(w)} \\
                 &= (\lambda + \theta w)^2 \; t \; e^{-\lambda t - \theta wt} \numberthis
                    \label{eq:bivar-cond-exp}
\end{align*}

From equation \ref{eq:bivar-cond-exp}, we can derive the conditional failure rate given a constant
load on the disk $w$ as follows:
\begin{align*}
    S_{T|W}(t|w) &= \int\limits_t^\infty f_{T|W}(t|w) dt \\
                 &= (\lambda +\theta w)^2 \int\limits_t^\infty \;t \;e^{-\lambda t -\theta wt} \;dt \\
                 &= (1 + (\lambda + \theta w)t) e^{-\lambda t - \theta wt} \\
\end{align*}
\begin{align*}
    h_{T|W}(t|w) &= \frac{f_{T|W}(t|w)}{S_{T|W}(t|w)} \\
                 &= \frac{t(\lambda + \theta w)^2}{1+(\lambda + \theta w)t} \numberthis
                 \label{eq:bivar-cond-h}
\end{align*}

The above equation starts at zero and approaches $\lambda + \theta w$ as $t$ grows. This shows that
failure rate is, in a way (in the limit at infinity), linearly dependent on the load on the disk,
$w$.

Stepping back, remember that hazard rate is needed in Importance Sampling methodology for rare
event simulation (section \ref{sec:stor-is-theory}). However, our derivation above is limited in
the sense that it assumes the disk experiences a constant disk load throughout its lifetime, but
this may not be the case. Consider a system at the start of the simulation when every disks are
working and are experiencing certain amounts of load. After the first failure, the load on all
remaining disks may change which would affect their failure patterns too. At this point, to
calculate the new conditional hazard rate, we need to first compute the new conditional bivariate
distribution using new random variables $T' = T - t_0$ and $X' = X - x_0$ (where $t_0, x_0$ is the
age and total I/O for a particular disk at that point in simulation) conditioned on the new
reachable subdomain ($T' > 0, X' > 0$). Then proceed to compute the conditional hazard rate based
on the new load. For exponential marginal distributions, the resulting expression will be exactly
the same as in equation \ref{eq:bivar-cond-h} except in $t'$ and $x'$ instead of $t$ and $x$. This
would mean that, when a failure occurs, every disk in the system will have their hazard rate reset
to zero (since $h_{T|W}(t|w)$ starts at zero). This behavior seems unexpected and we aren't
completely sure whether this is the right way to model this.

\section{Conclusions}

In this chapter, we presented a promising method to capture failure dependence on disk utilization.
Due to the mathematical complexity involved, we limited our discussion (as well as implementation)
to exponential distributions. Unfortunately, even under this simple assumption, the model seems
to have a few quirks that need to be resolved (such as resetting failure rates).

This theory was mostly based on speculations with little evidence. So NetApp jumped in and
generously provided us with real-world data for us to search for evidence of such behaviors.
In chapter \ref{ch:empirical}, we'll discuss our journey of empirical analysis in great detail.
